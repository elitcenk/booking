### Booking Justlife project

This project for justlife case study. Contains professionals booking REST services.

In this project
- ~~Spring~~
- ~~H2 Embedded~~
- ~~Lombok~~
- ~~Mapstruct~~
- ~~Actuator~~ 
- ~~Swagger~~
- ~~Docker~~ 
- ~~Flyway~~
- Repository Integration Test
- Rest mock test
- Update booking with multiple professionals
