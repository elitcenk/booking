package com.cenk.justlife.booking.controller;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.cenk.justlife.booking.controller.model.BookingDTO;
import com.cenk.justlife.booking.controller.model.BookingDuration;
import com.cenk.justlife.booking.controller.model.ProfessionalAvailableTimeDTO;
import com.cenk.justlife.booking.controller.model.ProfessionalDTO;
import com.cenk.justlife.booking.service.BookingService;
import com.cenk.justlife.booking.util.UrlConstants;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping (UrlConstants.URL_BOOKING)
public class BookingController {

	private final BookingService bookingService;

	@ApiOperation (value = "Create booking")
	//@formatter:off
	@ApiResponses (value = {
			@ApiResponse(code = 201, message = "Successfully created"),
			@ApiResponse(code = 400, message = "if the booking is not valid"),
	})
	//@formatter:on
	@PostMapping ("/")
	public ResponseEntity<BookingDTO> createBooking(@Valid @RequestBody BookingDTO booking) {
		BookingDTO result;
		try{
			result = bookingService.create(booking);
		} catch (IllegalArgumentException e){
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
		return ResponseEntity.status(HttpStatus.CREATED).body(result);
	}

	@ApiOperation (value = "Updates an existing booking")
	//@formatter:off
	@ApiResponses (value = {
			@ApiResponse(code = 200, message = "Successfully updated"),
			@ApiResponse(code = 400, message = "if the booking is not valid"),
	})
	//@formatter:on
	@PutMapping (produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<BookingDTO> updateBooking(@Valid @RequestBody BookingDTO booking){
		BookingDTO result;
		try{
			result = bookingService.update(booking);
		} catch (IllegalArgumentException e){
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
		return ResponseEntity.ok().body(result);
	}

	@ApiOperation (value = "Find all the bookings")
	//@formatter:off
	@ApiResponses (value = {
			@ApiResponse(code = 200, message = "Successfully retrieved"),
	})
	//@formatter:on
	@GetMapping (produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<BookingDTO>> findAll() {
		List<BookingDTO> dtos = bookingService.findAll();
		return ResponseEntity.ok().body(dtos);
	}

	@ApiOperation (value = "Find the id booking")
	//@formatter:off
	@ApiResponses (value = {
			@ApiResponse(code = 200, message = "Successfully retrieved"),
			@ApiResponse(code = 404, message = "Booking not found"),
	})
	//@formatter:on
	@GetMapping ("/{id}")
	public ResponseEntity<BookingDTO> findById(@PathVariable String id) {
		Optional<BookingDTO> optionalBooking = bookingService.findById(id);
		if(optionalBooking.isPresent()){
			return ResponseEntity.ok(optionalBooking.get());
		} else {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
	}

	@ApiOperation (value = "Delete the id booking")
	//@formatter:off
	@ApiResponses (value = {
			@ApiResponse(code = 204, message = "Successfully deleted"),
	})
	//@formatter:on
	@DeleteMapping ("/{id}")
	public ResponseEntity<Void> deleteBooking(@PathVariable String id) {
		bookingService.delete(id);
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}

	@ApiOperation (value = "Check availability by date")
	//@formatter:off
	@ApiResponses (value = {
			@ApiResponse(code = 202, message = "Successfully returned available professionals and available times.")
	})
	//@formatter:on
	@GetMapping ("/availability/checkbydate")
	public ResponseEntity<List<ProfessionalAvailableTimeDTO>> availabilityCheck(@RequestParam ("localDate") @DateTimeFormat (iso = DateTimeFormat.ISO.DATE) LocalDate localDate) {
		List<ProfessionalAvailableTimeDTO> professionalAvailableTimeDTOS = bookingService.availabilityCheck(localDate);
		return ResponseEntity.ok(professionalAvailableTimeDTOS);
	}

	/**
	 * {@code GET  /availability/checkbydate/} : check availability by date
	 *
	 * @param startDateTime start date time
	 * @param bookingDuration booking duration
	 * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the booking, or with status {@code 404 (Not Found)}.
	 */
	@ApiOperation (value = "Check availability by start datetime and duration")
	//@formatter:off
	@ApiResponses (value = {
			@ApiResponse(code = 202, message = "Successfully returned available professionals."),
	})
	//@formatter:on
	@GetMapping ("/availability/check")
	public ResponseEntity<List<ProfessionalDTO>> availabilityCheck(@RequestParam ("startDateTime") @DateTimeFormat (iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime startDateTime,
			@RequestParam ("bookingDuration") Integer bookingDuration) {
		List<ProfessionalDTO> professionalDTOS = bookingService.availabilityCheck(startDateTime, BookingDuration.byId(bookingDuration));
		return ResponseEntity.ok(professionalDTOS);
	}
}
