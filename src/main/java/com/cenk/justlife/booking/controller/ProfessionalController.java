package com.cenk.justlife.booking.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.cenk.justlife.booking.controller.model.ProfessionalDTO;
import com.cenk.justlife.booking.service.ProfessionalService;
import com.cenk.justlife.booking.util.UrlConstants;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping (UrlConstants.URL_PROFESSIONAL)
public class ProfessionalController {

	private final ProfessionalService professionalService;

	@ApiOperation (value = "Create professional")
	//@formatter:off
	@ApiResponses (value = {
			@ApiResponse(code = 201, message = "Successfully created"),
			@ApiResponse(code = 400, message = "if the professional is not valid"),
	})
	//@formatter:on
	@PostMapping ("/")
	public ResponseEntity<ProfessionalDTO> createProfessional(@Valid @RequestBody ProfessionalDTO professional) {
		ProfessionalDTO result = professionalService.create(professional);
		return ResponseEntity.ok().body(result);
	}

	@ApiOperation (value = "Updates an existing professional")
	//@formatter:off
	@ApiResponses (value = {
			@ApiResponse(code = 200, message = "Successfully updated"),
			@ApiResponse(code = 400, message = "if the professional is not valid"),
	})
	//@formatter:on
	@PutMapping (produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ProfessionalDTO> updateProfessional(@Valid @RequestBody ProfessionalDTO professional) {
		ProfessionalDTO result = professionalService.update(professional);
		return ResponseEntity.ok().body(result);
	}

	@ApiOperation (value = "Find all the professionals")
	//@formatter:off
	@ApiResponses (value = {
			@ApiResponse(code = 200, message = "Successfully retrieved"),
	})
	//@formatter:on
	@GetMapping (produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ProfessionalDTO>> findAll() {
		List<ProfessionalDTO> dtos = professionalService.findAll();
		return ResponseEntity.ok().body(dtos);
	}

	@ApiOperation (value = "Find the id professional")
	//@formatter:off
	@ApiResponses (value = {
			@ApiResponse(code = 200, message = "Successfully retrieved"),
			@ApiResponse(code = 404, message = "Professional not found"),
	})
	//@formatter:on
	@GetMapping ("/{id}")
	public ResponseEntity<ProfessionalDTO> findById(@PathVariable String id) {
		Optional<ProfessionalDTO> optionalProfessional = professionalService.findById(id);
		if(optionalProfessional.isPresent()){
			return ResponseEntity.ok(optionalProfessional.get());
		} else {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
	}

	@ApiOperation (value = "Delete the id professional")
	//@formatter:off
	@ApiResponses (value = {
			@ApiResponse(code = 204, message = "Successfully deleted"),
	})
	//@formatter:on
	@DeleteMapping ("/{id}")
	public ResponseEntity<Void> deleteProfessional(@PathVariable String id) {
		professionalService.delete(id);
		return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
	}

}
