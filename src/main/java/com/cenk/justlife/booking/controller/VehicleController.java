package com.cenk.justlife.booking.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.cenk.justlife.booking.controller.model.VehicleDTO;
import com.cenk.justlife.booking.service.VehicleService;
import com.cenk.justlife.booking.util.UrlConstants;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping (UrlConstants.URL_VEHICLE)
public class VehicleController {

	private final VehicleService vehicleService;

	@ApiOperation (value = "Create vehicle")
	//@formatter:off
	@ApiResponses (value = {
			@ApiResponse(code = 201, message = "Successfully created"),
			@ApiResponse(code = 400, message = "if the vehicle is not valid"),
	})
	//@formatter:on
	@PostMapping ("/")
	public ResponseEntity<VehicleDTO> createVehicle(@Valid @RequestBody VehicleDTO vehicle) {
		VehicleDTO result = vehicleService.create(vehicle);
		return ResponseEntity.ok().body(result);
	}

	@ApiOperation (value = "Updates an existing vehicle")
	//@formatter:off
	@ApiResponses (value = {
			@ApiResponse(code = 200, message = "Successfully updated"),
			@ApiResponse(code = 400, message = "if the vehicle is not valid"),
	})
	//@formatter:on
	@PutMapping (produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<VehicleDTO> updateVehicle(@Valid @RequestBody VehicleDTO vehicle) {
		VehicleDTO result = vehicleService.update(vehicle);
		return ResponseEntity.ok().body(result);
	}

	@ApiOperation (value = "Find all the vehicles")
	//@formatter:off
	@ApiResponses (value = {
			@ApiResponse(code = 200, message = "Successfully retrieved"),
	})
	//@formatter:on
	@GetMapping (produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<VehicleDTO>> findAll() {
		List<VehicleDTO> dtos = vehicleService.findAll();
		return ResponseEntity.ok().body(dtos);
	}

	@ApiOperation (value = "Find the id vehicle")
	//@formatter:off
	@ApiResponses (value = {
			@ApiResponse(code = 200, message = "Successfully retrieved"),
			@ApiResponse(code = 404, message = "Vehicle not found"),
	})
	//@formatter:on
	@GetMapping ("/{id}")
	public ResponseEntity<VehicleDTO> findById(@PathVariable String id) {
		Optional<VehicleDTO> optionalVehicle = vehicleService.findById(id);
		if(optionalVehicle.isPresent()){
			return ResponseEntity.ok(optionalVehicle.get());
		} else {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
	}

	@ApiOperation (value = "Delete the id vehicle")
	//@formatter:off
	@ApiResponses (value = {
			@ApiResponse(code = 204, message = "Successfully deleted"),
	})
	//@formatter:on
	@DeleteMapping ("/{id}")
	public ResponseEntity<Void> deleteVehicle(@PathVariable String id) {
		vehicleService.delete(id);
		return ResponseEntity.status(HttpStatus.ACCEPTED).build();
	}

}
