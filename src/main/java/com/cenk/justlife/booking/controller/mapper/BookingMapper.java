package com.cenk.justlife.booking.controller.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.cenk.justlife.booking.controller.model.BookingDTO;
import com.cenk.justlife.booking.dao.entity.Booking;

@Mapper
public interface BookingMapper {

	BookingMapper INSTANCE = Mappers.getMapper(BookingMapper.class);

	BookingDTO toDto(Booking booking);

	Booking toEntity(BookingDTO bookingDTO);
}
