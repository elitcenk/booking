package com.cenk.justlife.booking.controller.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.cenk.justlife.booking.controller.model.CustomerDTO;
import com.cenk.justlife.booking.dao.entity.Customer;

@Mapper
public interface CustomerMapper {

	CustomerMapper INSTANCE = Mappers.getMapper(CustomerMapper.class);

	CustomerDTO toDto(Customer customer);
}
