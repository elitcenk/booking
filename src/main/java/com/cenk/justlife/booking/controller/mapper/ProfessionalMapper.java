package com.cenk.justlife.booking.controller.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.cenk.justlife.booking.controller.model.ProfessionalDTO;
import com.cenk.justlife.booking.dao.entity.Professional;

@Mapper
public interface ProfessionalMapper {

	ProfessionalMapper INSTANCE = Mappers.getMapper(ProfessionalMapper.class);

	ProfessionalDTO toDto(Professional professional);

	Professional toEntity(ProfessionalDTO professionalDTO);
}
