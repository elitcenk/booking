package com.cenk.justlife.booking.controller.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.cenk.justlife.booking.controller.model.VehicleDTO;
import com.cenk.justlife.booking.dao.entity.Vehicle;

@Mapper
public interface VehicleMapper {

	VehicleMapper INSTANCE = Mappers.getMapper(VehicleMapper.class);

	VehicleDTO toDto(Vehicle vehicle);

	Vehicle toEntity(VehicleDTO vehicleDTO);
}
