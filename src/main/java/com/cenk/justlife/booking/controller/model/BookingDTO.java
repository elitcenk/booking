package com.cenk.justlife.booking.controller.model;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.cenk.justlife.booking.dao.entity.BookingStatus;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude (content = JsonInclude.Include.NON_NULL)
public class BookingDTO {

	protected String createdBy;
	protected Date createdDate;
	@NotNull
	private String id;
	private int version;
	@NotNull
	private LocalDateTime startDate;
	@NotNull
	private BookingDuration bookingDuration;
	@NotNull
	private BookingStatus bookingStatus;
	@NotNull
	private String vehicleId;
	@NotNull
	private String customerId;
	@NotNull
	@Size (min = 1, max = 3)
	private List<ProfessionalDTO> professionals;
}
