package com.cenk.justlife.booking.controller.model;

import java.time.Duration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum BookingDuration {

	TWO_HOUR(1, Duration.ofHours(2)),
	FOUR_HOUR(2, Duration.ofHours(4));

	@Getter
	private Integer id;

	@Getter
	private Duration duration;

	public static BookingDuration byId(Integer id) {
		for(BookingDuration bookingDuration : values()){
			if(bookingDuration.id.equals(id)){
				return bookingDuration;
			}
		}
		return null;
	}

}
