package com.cenk.justlife.booking.controller.model;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode (callSuper = false)
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude (content = JsonInclude.Include.NON_NULL)
public class ProfessionalDTO implements Serializable {

	protected String createdBy;
	protected Date createdDate;
	@NotNull
	private String id;
	private int version;
	private String name;
	private String surname;
	private String personNumber;
}
