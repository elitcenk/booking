package com.cenk.justlife.booking.controller.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude (content = JsonInclude.Include.NON_NULL)
public class VehicleAvailableTimeDTO implements Serializable {

	private VehicleDTO vehicle;

	@Builder.Default
	private List<AvailableTime> availableTimes = new ArrayList<>();

}
