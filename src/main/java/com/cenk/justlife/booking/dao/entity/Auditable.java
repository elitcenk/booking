package com.cenk.justlife.booking.dao.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import org.springframework.data.domain.Persistable;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.cenk.justlife.booking.dao.util.RepositoryConstants;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode (of = {"id"})
@MappedSuperclass
@EntityListeners (AuditingEntityListener.class)
@Data
public class Auditable<I extends Serializable> implements Persistable<I>, Serializable {

	public static final Integer INITIAL_VERSION = 0;
	public static final String ENABLED = "y";
	public static final String DISABLED = "n";
	private static final long serialVersionUID = 4715376422268555265L;
	// transient fields which specifies not persisted fields
	// default value is false
	@Transient
	protected boolean isNew;
	@Id
	@Column (nullable = false, unique = true, updatable = false)
	private I id;
	@Column (name = "version")
	private Integer version = INITIAL_VERSION;
	@Column (name = "created_by")
	private I createdBy;
	@Column (name = "created_date")
	private LocalDateTime createdDate;
	@Column (name = "last_updated_by")
	private I lastUpdatedBy;
	@Column (name = "last_updated_date")
	private LocalDateTime lastUpdatedDate;

	// soft deletion
	@Column (nullable = false)
	private String deleted = RepositoryConstants.NA;

	@Override
	public boolean isNew() {
		return isNew;
	}

	public void setDeleted(String deleted) {
		this.deleted = UUID.randomUUID().toString();
	}

	@Override
	public I getId() {
		return id;
	}

}
