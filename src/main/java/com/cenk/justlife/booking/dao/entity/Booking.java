package com.cenk.justlife.booking.dao.entity;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.cenk.justlife.booking.controller.model.BookingDuration;
import com.cenk.justlife.booking.dao.util.RepositoryConstants;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table (name = "booking", schema = "booking")
@Data
@Builder
@EqualsAndHashCode (callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@DynamicUpdate
@ToString (callSuper = true, exclude = {"vehicle", "customer"})
@SQLDelete (sql = "UPDATE booking.booking SET deleted = now(), version = version + 1 WHERE id = ? and version = ?")
@Where (clause = "deleted = " + RepositoryConstants.NA_ENTITY)
public class Booking extends Auditable<String> {

	private static final long serialVersionUID = 2524824535646069207L;

	//columns
	@Column
	private LocalDateTime startDate;

	@Column
	private LocalDateTime endDate;

	@Enumerated (EnumType.ORDINAL)
	private BookingDuration bookingDuration;

	@Enumerated (EnumType.ORDINAL)
	private BookingStatus bookingStatus;

	//associations
	@ManyToOne (fetch = FetchType.LAZY)
	@JoinColumn (nullable = false, name = "vehicle_id", insertable = false, updatable = false, foreignKey = @ForeignKey (name = "fk_vehicle_booking_vehicle_id"))
	private Vehicle vehicle;

	@ManyToOne (fetch = FetchType.LAZY)
	@JoinColumn (nullable = false, name = "customer_id", insertable = false, updatable = false, foreignKey = @ForeignKey (name = "fk_customer_booking_customer_id"))
	private Customer customer;

	@OneToMany (mappedBy = "booking")
	private List<BookingProfessional> bookingProfessionalList;

	// foreign keys
	@Column (name = "vehicle_id")
	private String vehicleId;

	@Column (name = "customer_id")
	private String customerId;
}
