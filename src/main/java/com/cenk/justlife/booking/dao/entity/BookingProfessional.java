package com.cenk.justlife.booking.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.cenk.justlife.booking.dao.util.RepositoryConstants;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table (name = "booking_professional", schema = "booking")
@Data
@Builder
@EqualsAndHashCode (callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@DynamicUpdate
@ToString (callSuper = true, exclude = {"booking", "professional"})
@SQLDelete (sql = "UPDATE booking.booking_professional SET deleted = now(), version = version + 1 WHERE id = ? and version = ?")
@Where (clause = "deleted = " + RepositoryConstants.NA_ENTITY)
public class BookingProfessional extends Auditable<String> {

	private static final long serialVersionUID = -2776152674939706028L;

	//columns

	//associations
	@ManyToOne (fetch = FetchType.LAZY)
	@JoinColumn (nullable = false, name = "booking_id", insertable = false, updatable = false, foreignKey = @ForeignKey (name = "fk_booking_booking_professional_booking_id"))
	private Booking booking;

	@ManyToOne (fetch = FetchType.LAZY)
	@JoinColumn (nullable = false, name = "professional_id", insertable = false, updatable = false, foreignKey = @ForeignKey (name = "fk_professional_booking_professional_professional_id"))
	private Professional professional;

	// foreign keys
	@Column (name = "booking_id")
	private String bookingId;

	@Column (name = "professional_id")
	private String professionalId;
}
