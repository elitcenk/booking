package com.cenk.justlife.booking.dao.entity;

public enum BookingStatus {

	RESERVED,
	CANCELED,
	DONE

}
