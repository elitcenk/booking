package com.cenk.justlife.booking.dao.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.cenk.justlife.booking.dao.util.RepositoryConstants;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table (name = "professional", schema = "booking")
@Data
@Builder
@EqualsAndHashCode (callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@DynamicUpdate
@ToString (callSuper = true, exclude = {"bookings"})
@SQLDelete (sql = "UPDATE booking.professional SET deleted = now(), version = version + 1 WHERE id = ? and version = ?")
@Where (clause = "deleted = " + RepositoryConstants.NA_ENTITY)
public class Professional extends Auditable<String> {
	//columns

	@Column
	private String name;
	@Column
	private String surname;
	@Column
	private String personNumber;

	//associations
	@OneToMany (fetch = FetchType.LAZY, mappedBy = "professional")
	private List<BookingProfessional> bookings;

}
