package com.cenk.justlife.booking.dao.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.cenk.justlife.booking.dao.util.RepositoryConstants;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table (name = "vehicle", schema = "booking")
@Data
@Builder
@EqualsAndHashCode (callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@DynamicUpdate
@ToString (callSuper = true, exclude = {"bookings"})
@SQLDelete (sql = "UPDATE booking.vehicle SET silinmis = now(), version = version + 1 WHERE id = ? and version = ?")
@Where (clause = "deleted = " + RepositoryConstants.NA_ENTITY)
public class Vehicle extends Auditable<String> {

	private static final long serialVersionUID = 2524824535646069207L;

	//columns

	@Column
	private int capacity;

	@Column
	private String model;

	//associations
	@OneToMany (fetch = FetchType.LAZY, mappedBy = "vehicle")
	private List<Booking> bookings;

}
