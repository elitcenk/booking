package com.cenk.justlife.booking.dao.repository.booking;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import com.cenk.justlife.booking.dao.entity.Booking;

@Repository
public interface BookingRepository extends JpaRepository<Booking, String>, QuerydslPredicateExecutor<Booking>, CustomBookingRepository {

}
