package com.cenk.justlife.booking.dao.repository.booking;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;

import com.cenk.justlife.booking.controller.mapper.BookingMapper;
import com.cenk.justlife.booking.controller.model.BookingDTO;
import com.cenk.justlife.booking.dao.entity.Booking;
import com.cenk.justlife.booking.dao.entity.QBooking;
import com.cenk.justlife.booking.dao.entity.QBookingProfessional;
import com.cenk.justlife.booking.dao.entity.QProfessional;
import com.cenk.justlife.booking.dao.util.RepositoryConstants;
import com.querydsl.jpa.JPQLQuery;

public class BookingRepositoryImpl extends QuerydslRepositorySupport implements CustomBookingRepository {

	public BookingRepositoryImpl(EntityManager entityManager) {
		super(Booking.class);
		setEntityManager(entityManager);
	}

	@Override
	public List<BookingDTO> findBookingByVehicleId(LocalDateTime startBookingDate, LocalDateTime endBookingDate, String vehicleId) {
		JPQLQuery<Booking> query = from(QBooking.booking).select(QBooking.booking)
				.where(QBooking.booking.startDate.lt(startBookingDate).or(QBooking.booking.endDate.gt(endBookingDate)).and(QBooking.booking.vehicleId.eq(vehicleId)));

		return query.fetch().stream().map(BookingMapper.INSTANCE::toDto).collect(Collectors.toList());
	}

	@Override
	public List<BookingDTO> findBookingByProfessionalId(LocalDateTime startBookingDate, LocalDateTime endBookingDate, String professionalId) {
		JPQLQuery<Booking> query = from(QBookingProfessional.bookingProfessional).select(QBooking.booking)
				.leftJoin(QBooking.booking)
				.on(QBooking.booking.id.eq(QBookingProfessional.bookingProfessional.bookingId).and(QBooking.booking.deleted.eq(RepositoryConstants.NA_ENTITY)))
				.where(QBooking.booking.startDate.lt(startBookingDate).or(QBooking.booking.endDate.gt(endBookingDate)).and(QProfessional.professional.id.eq(professionalId)));

		return query.fetch().stream().map(BookingMapper.INSTANCE::toDto).collect(Collectors.toList());
	}
}
