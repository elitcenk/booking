package com.cenk.justlife.booking.dao.repository.booking;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.repository.NoRepositoryBean;

import com.cenk.justlife.booking.controller.model.BookingDTO;

@NoRepositoryBean
public interface CustomBookingRepository {

	List<BookingDTO> findBookingByVehicleId(LocalDateTime startBookingDate, LocalDateTime endBookingDate, String vehicleId);

	List<BookingDTO> findBookingByProfessionalId(LocalDateTime startDate, LocalDateTime endDate, String professionalId);
}
