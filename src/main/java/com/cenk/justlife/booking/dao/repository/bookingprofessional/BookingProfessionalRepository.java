package com.cenk.justlife.booking.dao.repository.bookingprofessional;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import com.cenk.justlife.booking.dao.entity.BookingProfessional;

@Repository
public interface BookingProfessionalRepository extends JpaRepository<BookingProfessional, String>, QuerydslPredicateExecutor<BookingProfessional> {

	List<BookingProfessional> findByBookingId(String bookingId);

}
