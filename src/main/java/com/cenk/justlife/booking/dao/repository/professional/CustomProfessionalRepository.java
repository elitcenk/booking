package com.cenk.justlife.booking.dao.repository.professional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import org.springframework.data.repository.NoRepositoryBean;

import com.cenk.justlife.booking.controller.model.ProfessionalDTO;
import com.cenk.justlife.booking.dao.entity.Booking;
import com.cenk.justlife.booking.dao.entity.Professional;

@NoRepositoryBean
public interface CustomProfessionalRepository {

	List<ProfessionalDTO> findAvailableProfessional(LocalDateTime startBookingDate, LocalDateTime endBookingDate);

	Map<Professional, List<Booking>> findAllProfessionalWithBookings(LocalDate date);
}
