package com.cenk.justlife.booking.dao.repository.professional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import com.cenk.justlife.booking.dao.entity.Professional;

@Repository
public interface ProfessionalRepository extends JpaRepository<Professional, String>, QuerydslPredicateExecutor<Professional>, CustomProfessionalRepository {

}
