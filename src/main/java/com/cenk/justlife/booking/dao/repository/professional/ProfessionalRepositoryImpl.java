package com.cenk.justlife.booking.dao.repository.professional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;

import com.cenk.justlife.booking.controller.mapper.ProfessionalMapper;
import com.cenk.justlife.booking.controller.model.ProfessionalDTO;
import com.cenk.justlife.booking.dao.entity.Booking;
import com.cenk.justlife.booking.dao.entity.Professional;
import com.cenk.justlife.booking.dao.entity.QBooking;
import com.cenk.justlife.booking.dao.entity.QBookingProfessional;
import com.cenk.justlife.booking.dao.entity.QProfessional;
import com.cenk.justlife.booking.dao.util.RepositoryConstants;
import com.querydsl.core.Tuple;
import com.querydsl.core.group.GroupBy;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.JPQLQuery;

public class ProfessionalRepositoryImpl extends QuerydslRepositorySupport implements CustomProfessionalRepository {

	public ProfessionalRepositoryImpl(EntityManager entityManager) {
		super(Professional.class);
		setEntityManager(entityManager);
	}

	@Override
	public List<ProfessionalDTO> findAvailableProfessional(LocalDateTime startBookingDate, LocalDateTime endBookingDate) {
		JPQLQuery<Professional> query = from(QProfessional.professional).select(QProfessional.professional)
				.where(QProfessional.professional.id.notIn(JPAExpressions.select(QBookingProfessional.bookingProfessional.professionalId)
						.from(QBookingProfessional.bookingProfessional)
						.distinct()
						.innerJoin(QBooking.booking)
						.on(QBooking.booking.id.eq(QBookingProfessional.bookingProfessional.bookingId).and(QBooking.booking.deleted.eq(RepositoryConstants.NA_ENTITY)))
						.innerJoin(QProfessional.professional)
						.on(QProfessional.professional.id.eq(QBookingProfessional.bookingProfessional.professionalId).and(QProfessional.professional.deleted.eq(RepositoryConstants.NA_ENTITY)))
						.where(QBooking.booking.startDate.lt(startBookingDate).or(QBooking.booking.endDate.gt(endBookingDate)))));

		return query.fetch().stream().map(ProfessionalMapper.INSTANCE::toDto).collect(Collectors.toList());
	}

	@Override
	public Map<Professional, List<Booking>> findAllProfessionalWithBookings(LocalDate date) {
		LocalDateTime endOfDay = LocalDateTime.of(date, LocalTime.MAX);

		JPQLQuery<Tuple> query = from(QProfessional.professional).select(QProfessional.professional, QBooking.booking)
				.leftJoin(QBookingProfessional.bookingProfessional)
				.on(QBookingProfessional.bookingProfessional.professionalId.eq(QProfessional.professional.id).and(QBookingProfessional.bookingProfessional.deleted.eq(RepositoryConstants.NA_ENTITY)))
				.leftJoin(QBooking.booking)
				.on(QBooking.booking.id.eq(QBookingProfessional.bookingProfessional.bookingId)
						.and(QBooking.booking.deleted.eq(RepositoryConstants.NA_ENTITY))
						.and(QBooking.booking.startDate.after(date.atStartOfDay()))
						.and(QBooking.booking.endDate.before(endOfDay)));
		return query.transform(GroupBy.groupBy(QProfessional.professional).as(GroupBy.list(QBooking.booking)));
	}
}
