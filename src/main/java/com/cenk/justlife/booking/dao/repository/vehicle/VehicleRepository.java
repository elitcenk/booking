package com.cenk.justlife.booking.dao.repository.vehicle;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import com.cenk.justlife.booking.dao.entity.Vehicle;

@Repository
public interface VehicleRepository extends JpaRepository<Vehicle, String>, QuerydslPredicateExecutor<Vehicle>, CustomVehicleRepository {

}
