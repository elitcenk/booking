package com.cenk.justlife.booking.dao.repository.vehicle;

import javax.persistence.EntityManager;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;

import com.cenk.justlife.booking.dao.entity.Vehicle;

public class VehicleRepositoryImpl extends QuerydslRepositorySupport implements CustomVehicleRepository {

	public VehicleRepositoryImpl(EntityManager entityManager) {
		super(Vehicle.class);
		setEntityManager(entityManager);
	}
}
