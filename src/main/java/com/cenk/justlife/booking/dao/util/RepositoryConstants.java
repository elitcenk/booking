package com.cenk.justlife.booking.dao.util;

public class RepositoryConstants {

	public static final String NA = "NA";
	public static final String NA_ENTITY = "'" + NA + "'";

	private RepositoryConstants() {
	}
}
