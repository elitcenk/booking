package com.cenk.justlife.booking.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import com.cenk.justlife.booking.controller.model.BookingDTO;
import com.cenk.justlife.booking.controller.model.BookingDuration;
import com.cenk.justlife.booking.controller.model.ProfessionalAvailableTimeDTO;
import com.cenk.justlife.booking.controller.model.ProfessionalDTO;

public interface BookingService {

	List<ProfessionalDTO> availabilityCheck(LocalDateTime startBookingDate, BookingDuration bookingDuration);

	List<ProfessionalAvailableTimeDTO> availabilityCheck(LocalDate date);

	BookingDTO create(BookingDTO bookingDTO) throws IllegalArgumentException;

	BookingDTO update(BookingDTO bookingDTO) throws IllegalArgumentException;

	List<BookingDTO> findAll();

	Optional<BookingDTO> findById(String id);

	void delete(String id);
}
