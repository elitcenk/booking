package com.cenk.justlife.booking.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cenk.justlife.booking.controller.mapper.BookingMapper;
import com.cenk.justlife.booking.controller.model.AvailableTime;
import com.cenk.justlife.booking.controller.model.BookingDTO;
import com.cenk.justlife.booking.controller.model.BookingDuration;
import com.cenk.justlife.booking.controller.model.ProfessionalAvailableTimeDTO;
import com.cenk.justlife.booking.controller.model.ProfessionalDTO;
import com.cenk.justlife.booking.dao.entity.Booking;
import com.cenk.justlife.booking.dao.entity.BookingProfessional;
import com.cenk.justlife.booking.dao.repository.booking.BookingRepository;
import com.cenk.justlife.booking.dao.repository.bookingprofessional.BookingProfessionalRepository;
import com.cenk.justlife.booking.util.BookingUtil;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class BookingServiceImpl implements BookingService {

	private final BookingRepository bookingRepository;
	private final BookingProfessionalRepository bookingProfessionalRepository;
	private final ProfessionalService professionalService;

	@Override
	public List<ProfessionalDTO> availabilityCheck(LocalDateTime startBookingDate, BookingDuration bookingDuration) {
		LocalDateTime endBookingDate = startBookingDate.plus(bookingDuration.getDuration());
		if(BookingUtil.FREE_DAYS.contains(startBookingDate.toLocalDate().getDayOfWeek()) || BookingUtil.FREE_DAYS.contains(endBookingDate.toLocalDate().getDayOfWeek())){
			return new ArrayList<>();
		}

		if(startBookingDate.getHour() < BookingUtil.WORKING_START_HOUR || startBookingDate.getHour() >= BookingUtil.WORKING_END_HOUR){
			return new ArrayList<>();
		}
		if(endBookingDate.getHour() > BookingUtil.WORKING_END_HOUR || endBookingDate.getHour() <= BookingUtil.WORKING_START_HOUR){
			return new ArrayList<>();
		}

		startBookingDate = startBookingDate.minusMinutes(BookingUtil.BREAK_MINUTES);
		endBookingDate = endBookingDate.plusMinutes(BookingUtil.BREAK_MINUTES);

		return professionalService.findAvailableProfessional(startBookingDate, endBookingDate);
	}

	@Override
	public List<ProfessionalAvailableTimeDTO> availabilityCheck(LocalDate date) {
		Map<ProfessionalDTO, List<BookingDTO>> professionalBookings = professionalService.findAllProfessionalWithBookings(date);

		return professionalBookings.keySet().stream().map(professional -> {
			ProfessionalAvailableTimeDTO professionAvailableTime = ProfessionalAvailableTimeDTO.builder().professional(professional).build();
			LocalDateTime startDateTime = LocalDateTime.of(date, LocalTime.of(BookingUtil.WORKING_START_HOUR, 0));
			LocalDateTime endDateTime = LocalDateTime.of(date, LocalTime.of(BookingUtil.WORKING_END_HOUR, 0));
			for(BookingDTO booking : professionalBookings.get(professional)){
				endDateTime = booking.getStartDate().minusMinutes(BookingUtil.BREAK_MINUTES);
				if(!endDateTime.isAfter(startDateTime)){
					professionAvailableTime.getAvailableTimes().add(AvailableTime.builder().startDate(startDateTime).endDate(endDateTime).build());
				}
				startDateTime = booking.getStartDate().plus(booking.getBookingDuration().getDuration()).plusMinutes(BookingUtil.BREAK_MINUTES);
			}
			if(!endDateTime.isAfter(startDateTime)){
				professionAvailableTime.getAvailableTimes().add(AvailableTime.builder().startDate(startDateTime).endDate(endDateTime).build());
			}
			return professionAvailableTime;
		}).collect(Collectors.toList());
	}

	@Override
	@Transactional
	public BookingDTO create(BookingDTO bookingDTO) throws IllegalArgumentException {
		LocalDateTime endBookingDate = bookingDTO.getStartDate().plus(bookingDTO.getBookingDuration().getDuration());
		checkBookingDate(bookingDTO.getStartDate(), endBookingDate);

		List<String> professionalIds = bookingDTO.getProfessionals().stream().map(ProfessionalDTO::getId).collect(Collectors.toList());
		List<ProfessionalDTO> availableProfessionals = availabilityCheck(bookingDTO.getStartDate(), bookingDTO.getBookingDuration());
		boolean allContains = availableProfessionals.stream().map(ProfessionalDTO::getId).collect(Collectors.toList()).containsAll(professionalIds);
		if(!allContains){
			throw new IllegalArgumentException("All professional must be available.");
		}

		List<BookingDTO> vehicleBookings = bookingRepository.findBookingByVehicleId(bookingDTO.getStartDate(), endBookingDate, bookingDTO.getVehicleId());
		if(!vehicleBookings.isEmpty()){
			throw new IllegalArgumentException("Vehicle is not available");
		}

		Booking booking = BookingMapper.INSTANCE.toEntity(bookingDTO);
		booking.setId(UUID.randomUUID().toString());
		booking.setNew(true);
		Booking bookingSaved = bookingRepository.save(booking);

		List<BookingProfessional> bookingProfessionals = professionalIds.stream().map(professionalId -> {
			BookingProfessional bookingProfessional = BookingProfessional.builder().bookingId(bookingSaved.getId()).professionalId(professionalId).build();
			bookingProfessional.setId(UUID.randomUUID().toString());
			bookingProfessional.setNew(true);
			return bookingProfessional;
		}).collect(Collectors.toList());

		bookingProfessionalRepository.saveAll(bookingProfessionals);

		return BookingMapper.INSTANCE.toDto(bookingSaved);
	}

	private void checkBookingDate(LocalDateTime startBookingDate, LocalDateTime endBookingDate) throws IllegalArgumentException {
		if(BookingUtil.FREE_DAYS.contains(startBookingDate.toLocalDate().getDayOfWeek()) || BookingUtil.FREE_DAYS.contains(endBookingDate.toLocalDate().getDayOfWeek())){
			throw new IllegalArgumentException("Booking date contains free days.");
		}

		if(startBookingDate.getHour() < BookingUtil.WORKING_START_HOUR || startBookingDate.getHour() >= BookingUtil.WORKING_END_HOUR){
			throw new IllegalArgumentException("Start time is not appropriate.");
		}
		if(endBookingDate.getHour() > BookingUtil.WORKING_END_HOUR || endBookingDate.getHour() <= BookingUtil.WORKING_START_HOUR){
			throw new IllegalArgumentException("End time is not appropriate.");
		}
	}

	@Override
	@Transactional
	public BookingDTO update(BookingDTO bookingDTO) throws IllegalArgumentException {
		LocalDateTime endBookingDate = bookingDTO.getStartDate().plus(bookingDTO.getBookingDuration().getDuration());
		checkBookingDate(bookingDTO.getStartDate(), endBookingDate);

		List<BookingDTO> vehicleBookings = bookingRepository.findBookingByVehicleId(bookingDTO.getStartDate(), endBookingDate, bookingDTO.getVehicleId());
		if(!vehicleBookings.isEmpty() && vehicleBookings.stream().noneMatch(bookingDTO1 -> bookingDTO1.getId().equals(bookingDTO.getId()))){
			throw new IllegalArgumentException("Vehicle is not available");
		}

		updateBookingProfessional(bookingDTO);

		Booking save = bookingRepository.save(BookingMapper.INSTANCE.toEntity(bookingDTO));
		return BookingMapper.INSTANCE.toDto(save);
	}

	private void updateBookingProfessional(BookingDTO bookingDTO) throws IllegalArgumentException {
		Optional<Booking> booking = bookingRepository.findById(bookingDTO.getId());
		if(!booking.isPresent()){
			throw new IllegalArgumentException("Booking not found");
		}
		List<BookingProfessional> oldProfessionals = booking.get().getBookingProfessionalList();
		List<BookingProfessional> added = bookingDTO.getProfessionals()
				.stream()
				.filter(professionalDTO -> oldProfessionals.stream().noneMatch(bookingProfessional -> bookingProfessional.getProfessionalId().equals(professionalDTO.getId())))
				.map(professionalDTO -> {
					BookingProfessional bookingProfessional = BookingProfessional.builder().professionalId(professionalDTO.getId()).bookingId(bookingDTO.getId()).build();
					bookingProfessional.setId(UUID.randomUUID().toString());
					bookingProfessional.setNew(true);
					return bookingProfessional;
				})
				.collect(Collectors.toList());

		List<BookingProfessional> deleted = oldProfessionals.stream()
				.filter(professionalDTO -> bookingDTO.getProfessionals().stream().noneMatch(professionalDTO1 -> professionalDTO1.getId().equals(professionalDTO.getId())))
				.collect(Collectors.toList());
		bookingProfessionalRepository.deleteAll(deleted);
		bookingProfessionalRepository.saveAll(added);
	}

	@Override
	public List<BookingDTO> findAll() {
		List<Booking> bookings = bookingRepository.findAll();
		return bookings.stream().map(BookingMapper.INSTANCE::toDto).collect(Collectors.toList());
	}

	@Override
	public Optional<BookingDTO> findById(String id) {
		Optional<Booking> optionalBooking = bookingRepository.findById(id);
		return optionalBooking.map(BookingMapper.INSTANCE::toDto);
	}

	@Override
	@Transactional
	public void delete(String id) {
		bookingRepository.deleteById(id);
	}
}
