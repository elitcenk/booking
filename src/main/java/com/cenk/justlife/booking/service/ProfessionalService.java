package com.cenk.justlife.booking.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.transaction.annotation.Transactional;

import com.cenk.justlife.booking.controller.model.BookingDTO;
import com.cenk.justlife.booking.controller.model.ProfessionalDTO;

public interface ProfessionalService {

	ProfessionalDTO create(ProfessionalDTO professionalDTO);

	ProfessionalDTO update(ProfessionalDTO professionalDTO);

	Map<ProfessionalDTO, List<BookingDTO>> findAllProfessionalWithBookings(LocalDate date);

	List<ProfessionalDTO> findAvailableProfessional(LocalDateTime startBookingDate, LocalDateTime endBookingDate);

	List<ProfessionalDTO> findAll();

	Optional<ProfessionalDTO> findById(String id);

	@Transactional
	void delete(String id);
}
