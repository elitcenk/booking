package com.cenk.justlife.booking.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cenk.justlife.booking.controller.mapper.BookingMapper;
import com.cenk.justlife.booking.controller.mapper.ProfessionalMapper;
import com.cenk.justlife.booking.controller.model.BookingDTO;
import com.cenk.justlife.booking.controller.model.ProfessionalDTO;
import com.cenk.justlife.booking.dao.entity.Booking;
import com.cenk.justlife.booking.dao.entity.Professional;
import com.cenk.justlife.booking.dao.repository.professional.ProfessionalRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ProfessionalServiceImpl implements ProfessionalService {

	private final ProfessionalRepository professionalRepository;

	@Override
	public ProfessionalDTO create(ProfessionalDTO professionalDTO) {
		Professional professional = ProfessionalMapper.INSTANCE.toEntity(professionalDTO);
		professional.setNew(true);
		professional.setId(UUID.randomUUID().toString());
		Professional save = professionalRepository.save(professional);
		return ProfessionalMapper.INSTANCE.toDto(save);
	}

	@Override
	public ProfessionalDTO update(ProfessionalDTO professionalDTO) {
		Professional professional = ProfessionalMapper.INSTANCE.toEntity(professionalDTO);
		Professional save = professionalRepository.save(professional);
		return ProfessionalMapper.INSTANCE.toDto(save);
	}

	@Override
	public Map<ProfessionalDTO, List<BookingDTO>> findAllProfessionalWithBookings(LocalDate date) {
		Map<Professional, List<Booking>> allProfessionalWithBookings = professionalRepository.findAllProfessionalWithBookings(date);

		Map<ProfessionalDTO, List<BookingDTO>> allProfessionalDTOBooking = new HashMap<>();
		allProfessionalWithBookings.forEach((key, value) -> {
			ProfessionalDTO professionalDTO = ProfessionalMapper.INSTANCE.toDto(key);
			List<BookingDTO> bookingDTOS = value.stream().map(BookingMapper.INSTANCE::toDto).collect(Collectors.toList());
			allProfessionalDTOBooking.put(professionalDTO, bookingDTOS);
		});
		return allProfessionalDTOBooking;
	}

	@Override
	public List<ProfessionalDTO> findAvailableProfessional(LocalDateTime startBookingDate, LocalDateTime endBookingDate) {
		return professionalRepository.findAvailableProfessional(startBookingDate, endBookingDate);
	}

	@Override
	public List<ProfessionalDTO> findAll() {
		List<Professional> professionals = professionalRepository.findAll();
		return professionals.stream().map(ProfessionalMapper.INSTANCE::toDto).collect(Collectors.toList());
	}

	@Override
	public Optional<ProfessionalDTO> findById(String id) {
		Optional<Professional> optionalProfessional = professionalRepository.findById(id);
		return optionalProfessional.map(ProfessionalMapper.INSTANCE::toDto);
	}

	@Override
	@Transactional
	public void delete(String id) {
		professionalRepository.deleteById(id);
	}
}
