package com.cenk.justlife.booking.service;

import java.util.List;
import java.util.Optional;

import org.springframework.transaction.annotation.Transactional;

import com.cenk.justlife.booking.controller.model.VehicleDTO;

public interface VehicleService {

	VehicleDTO create(VehicleDTO vehicleDTO);

	VehicleDTO update(VehicleDTO vehicleDTO);

	List<VehicleDTO> findAll();

	Optional<VehicleDTO> findById(String id);

	@Transactional
	void delete(String id);
}
