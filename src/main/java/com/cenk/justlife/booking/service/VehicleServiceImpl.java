package com.cenk.justlife.booking.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cenk.justlife.booking.controller.mapper.VehicleMapper;
import com.cenk.justlife.booking.controller.model.VehicleDTO;
import com.cenk.justlife.booking.dao.entity.Vehicle;
import com.cenk.justlife.booking.dao.repository.vehicle.VehicleRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class VehicleServiceImpl implements VehicleService {

	private final VehicleRepository vehicleRepository;

	@Override
	public VehicleDTO create(VehicleDTO vehicleDTO) {
		Vehicle vehicle = VehicleMapper.INSTANCE.toEntity(vehicleDTO);
		vehicle.setNew(true);
		vehicle.setId(UUID.randomUUID().toString());
		Vehicle save = vehicleRepository.save(vehicle);
		return VehicleMapper.INSTANCE.toDto(save);
	}

	@Override
	public VehicleDTO update(VehicleDTO vehicleDTO) {
		Vehicle vehicle = VehicleMapper.INSTANCE.toEntity(vehicleDTO);
		Vehicle save = vehicleRepository.save(vehicle);
		return VehicleMapper.INSTANCE.toDto(save);
	}

	@Override
	public List<VehicleDTO> findAll() {
		List<Vehicle> vehicles = vehicleRepository.findAll();
		return vehicles.stream().map(VehicleMapper.INSTANCE::toDto).collect(Collectors.toList());
	}

	@Override
	public Optional<VehicleDTO> findById(String id) {
		Optional<Vehicle> optionalVehicle = vehicleRepository.findById(id);
		return optionalVehicle.map(VehicleMapper.INSTANCE::toDto);
	}

	@Override
	@Transactional
	public void delete(String id) {
		vehicleRepository.deleteById(id);
	}
}
