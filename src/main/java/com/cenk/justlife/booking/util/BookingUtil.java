package com.cenk.justlife.booking.util;

import java.time.DayOfWeek;
import java.util.Collection;
import java.util.Collections;

public class BookingUtil {

	public static final int WORKING_START_HOUR = 8;
	public static final int WORKING_END_HOUR = 22;
	public static final int BREAK_MINUTES = 30;
	public static final Collection<DayOfWeek> FREE_DAYS = Collections.singletonList(DayOfWeek.FRIDAY);

	private BookingUtil() {
	}
}
