package com.cenk.justlife.booking.util;

public class UrlConstants {

	// MAIN API URLS START //
	private static final String URL_API_V1 = "/api/v1";
	public static final String URL_BOOKING = URL_API_V1 + "/booking";
	public static final String URL_PROFESSIONAL = URL_API_V1 + "/professional";
	public static final String URL_VEHICLE = URL_API_V1 + "/vehicle";

	private UrlConstants() {
	}
}
