create schema booking;

drop table if exists booking.booking CASCADE;
drop table if exists booking.booking_professional CASCADE;
drop table if exists booking.customer CASCADE;
drop table if exists booking.professional CASCADE;
drop table if exists booking.vehicle CASCADE;
create table booking.booking
(
    id                varchar(255) not null,
    created_by        varchar(255),
    created_date      timestamp,
    deleted           varchar(255) not null,
    last_updated_by   varchar(255),
    last_updated_date timestamp,
    version           integer,
    booking_duration  integer,
    booking_status    integer,
    customer_id       varchar(255),
    end_date          timestamp,
    start_date        timestamp,
    vehicle_id        varchar(255),
    primary key (id)
);
create table booking.booking_professional
(
    id                varchar(255) not null,
    created_by        varchar(255),
    created_date      timestamp,
    deleted           varchar(255) not null,
    last_updated_by   varchar(255),
    last_updated_date timestamp,
    version           integer,
    booking_id        varchar(255),
    professional_id   varchar(255),
    primary key (id)
);
create table booking.customer
(
    id                varchar(255) not null,
    created_by        varchar(255),
    created_date      timestamp,
    deleted           varchar(255) not null,
    last_updated_by   varchar(255),
    last_updated_date timestamp,
    version           integer,
    name              varchar(255),
    person_id         varchar(255),
    surname           varchar(255),
    primary key (id)
);
create table booking.professional
(
    id                varchar(255) not null,
    created_by        varchar(255),
    created_date      timestamp,
    deleted           varchar(255) not null,
    last_updated_by   varchar(255),
    last_updated_date timestamp,
    version           integer,
    name              varchar(255),
    person_number     varchar(255),
    surname           varchar(255),
    primary key (id)
);
create table booking.vehicle
(
    id                varchar(255) not null,
    created_by        varchar(255),
    created_date      timestamp,
    deleted           varchar(255) not null,
    last_updated_by   varchar(255),
    last_updated_date timestamp,
    version           integer,
    capacity          integer,
    model             varchar(255),
    primary key (id)
);
alter table booking.booking
    add constraint fk_customer_booking_customer_id foreign key (customer_id) references booking.customer;
alter table booking.booking
    add constraint fk_vehicle_booking_vehicle_id foreign key (vehicle_id) references booking.vehicle;
alter table booking.booking_professional
    add constraint fk_booking_booking_professional_booking_id foreign key (booking_id) references booking.booking;
alter table booking.booking_professional
    add constraint fk_professional_booking_professional_professional_id foreign key (professional_id) references booking.professional
