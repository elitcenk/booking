insert into booking.professional (deleted, version, name, person_number, surname, id)
values ('NA', 0, 'Cenk', '111111', 'Alp', '1');

insert into booking.professional (deleted, version, name, person_number, surname, id)
values ('NA', 0, 'Ahmet', '222222', 'Alp', '2');

insert into booking.professional (deleted, version, name, person_number, surname, id)
values ('NA', 0, 'Kemal', '333333', 'Alp', '3');

insert into booking.professional (deleted, version, name, person_number, surname, id)
values ('NA', 0, 'Hakkı', '444444', 'Alp', '4');

insert into booking.professional (deleted, version, name, person_number, surname, id)
values ('NA', 0, 'Simge', '555555', 'Alp', '5');


insert into booking.customer (deleted, version, name, person_id, surname, id)
values ('NA', 0, 'Hakkı', '666666', 'Yılmaz', '1');

insert into booking.vehicle (deleted, version, model, capacity, id)
values ('NA', 0, 'Renault Clio', 5, '1');

insert into booking.vehicle (deleted, version, model, capacity, id)
values ('NA', 0, 'Chevrolet ', 5, '2');

insert into booking.vehicle (deleted, version, model, capacity, id)
values ('NA', 0, 'Ikarus', 20, '3');

insert into booking.vehicle (deleted, version, model, capacity, id)
values ('NA', 0, 'Subaru Impreza', 2, '4');

insert into booking.vehicle (deleted, version, model, capacity, id)
values ('NA', 0, 'Renault Fluence', 5, '5');
