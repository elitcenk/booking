package com.cenk.justlife.booking;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.cenk.justlife.booking.initializer.BookingApplication;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@DisplayName ("Application Health Test")
@ExtendWith (SpringExtension.class)
@SpringBootTest (classes = {BookingApplication.class})
public class BookingApplicationTests {

	@Autowired
	private ApplicationContext applicationContext;

	@Test
	@DisplayName ("Context load test")
	public void contextLoads() {
		if(log.isInfoEnabled()){
			log.info(applicationContext.getId());
		}
	}

}
